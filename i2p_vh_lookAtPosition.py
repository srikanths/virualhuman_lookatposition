import sys
sys.path.append('../gen-py')
sys.path.append("../i2p/tools/py")

#for cerevoice
import Feedback.AgentFeedbackService as Feedback_Service
from Feedback.ttypes import *
import Feedback.constants

import EventPublisher.WorldQuery as Location_service
#import EventPublisher.EventSubscription #as Location_service
from EventPublisher.ttypes import *
import EventPublisher.constants


from I2P import constants
from I2P.ttypes import *
import Control
from Control import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

import ThriftTools

import sys, os, random
from PyQt4.QtCore import *
import PyQt4.QtGui as Gui
import json

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.figure import Figure



# Make socket
transportQ = TSocket.TSocket('localhost', 9090)
# Buffering is critical. Raw sockets are very slow
transportQ = TTransport.TBufferedTransport(transportQ)
# Wrap in a protocol
protocolQ = TBinaryProtocol.TBinaryProtocol(transportQ)
#Create a client to use the protocol encoder
clientQ = AgentControl.Client(protocolQ)#WorldQuery.Client(protocolQ)
#clientQ = WorldQuery.Client(protocolQ)
# Connect!
transportQ.open()

personName = 'Subject'

Emotions = ['HOPE','FEAR','SATISFACTION','FEAR_CONFORMED','DISAPPOINTMENT','RELIEF','JOY','DISTRESS','GRATIFICATION','REMORSE','PRIDE','SHAME']
Animations = ['WAVE_HAND','SHAKE_HANDS','STAND_UP','SIT_DOWN','ROCK','PAPER','SCISSOR']
Face_Expressions = ['NEUTRAL','ANGER','DISGUST','FEAR','HAPPINESS','SADNESS','SURPRISE']


class FeedbackHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
    def speakBegin(self, timestamp):
        print 'speakBegin', timestamp
        return
    def speakEnd(self, timestamp):
        print 'speakEnd', timestamp        
        return

class WorldQueryHandler:
    def __init__(self):
        print("Constructor")
        return
        
    def __del__(self):
        return
    def getLocation(self, agentName):
        print 'getlocation'
	arg1=Vec3()
        arg1.x=1
        arg1.y=2
        arg1.z=3
        return arg1
		
class AppForm(Gui.QMainWindow):
    def __init__(self, parent=None):
        Gui.QMainWindow.__init__(self, parent)
        self.setWindowTitle('Demo: PyQt with matplotlib')

        self.create_menu()
        self.create_main_frame()
        self.create_status_bar()

        self.on_draw()

        # Make socket
        self.transport = TSocket.TSocket('localhost', 9090)

        # Buffering is critical. Raw sockets are very slow
        self.transport = TTransport.TBufferedTransport(self.transport)

        # Wrap in a protocol
        self.protocol = TBinaryProtocol.TBinaryProtocol(self.transport)

        #Create a client to use the protocol encoder
        self.client = AgentControl.Client(self.protocol)

        # Connect!
        self.transport.open()
        
    def on_about(self):
        msg = """ A demo of using PyQt with matplotlib:
v        
         * Use the matplotlib navigation bar
         * Show or hide the grid
        """
        Gui.QMessageBox.about(self, "About the demo", msg.strip())

    def on_pick(self, event):
        currentText = str(self.operations.currentText())
        if currentText in self.controls["Clickable"]:
            position = Vec3()
            position.x = event.xdata
            position.y = 0
            position.z = event.ydata
            # self.client.lookAtPosition((event.xdata, event.ydata))
            print("TTTTTTTTTTTTT")
            currentText = str(self.operations.currentText())
            func = getattr(self.client, currentText)
            func(position)
            print event.xdata, event.ydata

    def on_draw(self):
        """ Redraws the figure
        """
        # clear the axes and redraw the plot anew
        #
        self.axes.clear()        
        self.axes.grid(self.grid_cb.isChecked())        
        self.canvas.draw()

    def onClick(self):
        """ We make the corresponding thrift call
        """
        currentText = str(self.operations.currentText())
        if "string" not in self.controls["Operations"][currentText].keys():
            arg = Vec3()
        else:
            arg = ''
            volume = 0
        # self.client.lookAtPosition((event.xdata, event.ydata))
        for i in range(self.control_box.count()):
            for j in range(self.control_box.itemAt(i).count()):
                widget = self.control_box.itemAt(i).itemAt(j).widget()
                # print widget.objectName()
                if widget.objectName() in self.controls["Operations"][currentText].keys():
                    print widget.objectName()
                    try:
                        print widget.text()
                        arg = str(widget.text())
                    except AttributeError:
                        print widget.value()
                        if str(widget.objectName()) == "volume":
                            volume =  widget.value()
                        elif str(widget.objectName()) == "Intensity":
                            Intensity =  widget.value()
                        else:    
                            setattr(arg, str(widget.objectName()[1]), \
                                widget.value())
                    except:
                        print 'not able to handle'
        #if "string" not in self.controls["Operations"][currentText].keys():
            #(arg.y, arg.z) = (arg.z, arg.y)
        print arg
        func = getattr(self.client, currentText)
        print func
        if currentText == 'speak':
            func(arg, volume)
        elif currentText == 'endLookAt':
            func()
        elif currentText == 'endPointAt':
            func()
        elif currentText == 'setFaceExpression':
            func(Face_Expressions.index(arg), Intensity)
        elif currentText == 'setEmotionalState':
            func(Emotions.index(arg), Intensity)
        elif currentText == 'playAnimation':
            func(Control.ttypes.Animation._NAMES_TO_VALUES[arg])   
        else:
            func(arg)
        
    def on_change(self):
        """ On change of QComboBox index, redraw the control_box
        """
        # clear the axes and redraw the plot anew
        #
        for iLayout in range(self.control_box.count()):
            # the first option is not clean
            # self.control_box.removeItem(self.control_box.itemAt(iLayout))
            for iWidget in range(self.control_box.itemAt(iLayout).count()):
                self.control_box.itemAt(iLayout).itemAt(iWidget).widget().close()
        self.controlBoxLayout()
        
    def controlBoxLayout(self):
        currentText = str(self.operations.currentText())
        for field in self.controls['Operations'][currentText]:
            blockLayout = Gui.QHBoxLayout()
            label = Gui.QLabel(field)
            gui_layout = getattr(Gui, self.controls['Variables'][field])
            setattr(self, field, gui_layout())
            widget = getattr(self, field)
            widget.setObjectName(field)
            blockLayout.addWidget(label)
            if (self.controls['Variables'][field] == 'QSlider'):
                slider = getattr(self, field)
                slider.setOrientation(Qt.Horizontal)
                slider.setMinimum(self.controls['Operations'][currentText][field][0])
                slider.setMaximum(self.controls['Operations'][currentText][field][1])
                sliderLabel = Gui.QLabel(str(slider.value()))
                self.connect(slider, SIGNAL('valueChanged(int)'), sliderLabel, SLOT('setNum(int)'))
                blockLayout.addWidget(getattr(self, field))
                blockLayout.addWidget(sliderLabel)
            else:
                blockLayout.addWidget(getattr(self, field))
            self.control_box.addLayout(blockLayout)

    def create_main_frame(self):
        self.main_frame = Gui.QWidget()
        
        # Create the mpl Figure and FigCanvas objects. 
        # 5x4 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = Figure((5.0, 4.0), dpi=self.dpi)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)
        
        # Since we have only one plot, we can use add_axes 
        # instead of add_subplot, but then the subplot
        # configuration tool in the navigation toolbar wouldn't
        # work.
        #
        self.axes = self.fig.add_subplot(111)
        self.axes.set_xlim((-250, 250))
        self.axes.set_ylim((-250, 250))
        # Bind the 'pick' event for clicking on one of the bars
        #
        self.canvas.mpl_connect('button_press_event', self.on_pick)
        
        # Create the navigation toolbar, tied to the canvas
        #
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)

        self.grid_cb = Gui.QCheckBox("Show &Grid")
        self.grid_cb.setChecked(True)
        self.connect(self.grid_cb, SIGNAL('stateChanged(int)'), self.on_draw)
        
        #
        # Layout with box sizers
        # 
        hbox = Gui.QHBoxLayout()
        
        for w in [ self.grid_cb ]:
            hbox.addWidget(w)
            hbox.setAlignment(w, Qt.AlignVCenter)

        # Reading controls from json file
        self.controls = json.load(open('controls.json'))

        # Dropdown for all actions
        self.operations = Gui.QComboBox()
        
        for operation in self.controls['Operations']:
            # list_item = Gui.QListWidgetItem(operation['Name'])
            self.operations.addItem(operation)        

        self.connect(self.operations, SIGNAL('currentIndexChanged(int)'), self.on_change)        

        self.control_box = Gui.QVBoxLayout()
        self.controlBoxLayout()

        execButton = Gui.QPushButton('Execute command')
        self.connect(execButton, SIGNAL('pressed()'), self.onClick)
        papaBox = Gui.QHBoxLayout()
        papaBox.addLayout(self.control_box)
        papaBox.addWidget(execButton)

        vbox = Gui.QVBoxLayout()
        vbox.addWidget(self.operations)
        vbox.addWidget(self.canvas)
        vbox.addWidget(self.mpl_toolbar)
        vbox.addLayout(hbox)
        vbox.addLayout(papaBox)
        
        self.main_frame.setLayout(vbox)
        self.setCentralWidget(self.main_frame)
    
    def create_status_bar(self):
        self.status_text = Gui.QLabel("This is a demo")
        self.statusBar().addWidget(self.status_text, 1)
        
    def create_menu(self):        
        quit_action = self.create_action("&Quit", slot=self.close, 
            shortcut="Ctrl+Q", tip="Close the application")
        
        self.help_menu = self.menuBar().addMenu("&Help")
        about_action = self.create_action("&About", 
            shortcut='F1', slot=self.on_about, 
            tip='About the demo')
        
        self.add_actions(self.help_menu, (about_action,))

    def add_actions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def create_action(  self, text, slot=None, shortcut=None, 
                        icon=None, tip=None, checkable=False, 
                        signal="triggered()"):
        action = Gui.QAction(text, self)
        if icon is not None:
            action.setIcon(Gui.QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        return action

def main():
    arg=Vec3()
    arg.x=1
    arg.y=2
    arg.z=3
    clientQ.lookAtPosition(arg)
    app = Gui.QApplication(sys.argv)
    form = AppForm()
    form.show()
    app.exec_()

def startFeedbackService():
    #starting feedback service
    feedback_handler = FeedbackHandler()
    location_handler = WorldQueryHandler()
    feedback_server = ThriftTools.ThriftServerThread(Feedback.constants.DEFAULT_AGENT_FEEDBACK_SERVICE_PORT,\
                                                     Feedback_Service,feedback_handler,'Agent Feedback Service','localhost')
    feedback_server.start()

    location_server = ThriftTools.ThriftServerThread(EventPublisher.constants.DEFAULT_WORLD_QUERY_PORT,\
                                                     Location_service,location_handler,'Agent location Service','localhost')
    location_server.start()
	
    


if __name__ == "__main__":
    startFeedbackService()
    main()
